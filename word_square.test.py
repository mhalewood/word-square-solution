import unittest
import word_square


# These tests are run against main function and will return a correct solution
class SolutionMethods(unittest.TestCase):
    '''
    this test is a quick test to ensure the correct response is given when a
    small number of letters are passed into the solve function'''
    def test_solve_with_length_2(self):
        self.assertEqual(
            word_square.solve(2, 'haah'),
            ['ah', 'ha'])

    '''
    this test checks that the solution returns the same response as the
    example provided in the brief'''
    def test_solve_example(self):
        self.assertEqual(
            word_square.solve(4, 'eeeeddoonnnsssrv'),
            ['rose', 'oven', 'send', 'ends'])

    '''
    this test checks that the solution returns the same response as the
    challenge provided '''
    def test_solve_challenge1(self):
        self.assertEqual(
            word_square.solve(4, 'aaccdeeeemmnnnoo'),
            ['moan', 'once', 'acme', 'need'])


# These tests are run against the function which recieves the words from the
# text file and should return a list of all words of set length
class Recieve_Words(unittest.TestCase):
    # this checks that all 2 letter words are retruned when calling the method
    def test_2letter_words(self):
        self.assertEqual(word_square.get_length_english_words(2),
                         ['aa', 'ab', 'ad', 'ae', 'ag', 'ah', 'ai', 'al', 'am',
                          'an', 'ar', 'as', 'at', 'aw', 'ax', 'ay', 'ba', 'be',
                          'bi', 'bo', 'by', 'de', 'do', 'ed', 'ef', 'eh', 'el',
                          'em', 'en', 'er', 'es', 'et', 'ex', 'fa', 'go', 'ha',
                          'he', 'hi', 'hm', 'ho', 'id', 'if', 'in', 'is', 'it',
                          'jo', 'ka', 'la', 'li', 'lo', 'ma', 'me', 'mi', 'mm',
                          'mo', 'mu', 'my', 'na', 'ne', 'no', 'nu', 'od', 'oe',
                          'of', 'oh', 'om', 'on', 'op', 'or', 'os', 'ow', 'ox',
                          'oy', 'pa', 'pe', 'pi', 're', 'sh', 'si', 'so', 'ta',
                          'ti', 'to', 'uh', 'um', 'un', 'up', 'us', 'ut', 'we',
                          'wo', 'xi', 'xu', 'ya', 'ye', 'yo'])


class Filter_Words(unittest.TestCase):
    def test_2letter_words(self):
        self.assertEqual(word_square.filter_words(
            ['ab', 'ah', 'ha'], 'haah', ''),
            ['ah', 'ha'])

    def test_prefix_words(self):
        self.assertEqual(word_square.filter_words(
            ['test', 'this', 'code', 'oven'], 'testhicodeven', 't'),
            ['test', 'this']
        )


class Remove_Characters(unittest.TestCase):
    def test_removing_characters(self):
        self.assertEqual(word_square.remove_characters(
            ['test', 'case'], 'testcaseone'), 'one')


class Potential_words(unittest.TestCase):
    def test_next_word_moan(self):
        self.assertEqual(word_square.find_potential_next_words(
            ['moan'], ['aced', 'aeon', 'amen', 'came', 'odea', 'omen', 'once'],
            'aaccdeeeemmnnnoo'), ['odea', 'omen', 'once'])


if __name__ == "__main__":
    unittest.main()

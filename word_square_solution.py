from word_square import solve
from datetime import datetime

challenge_cases = [[4, "aaccdeeeemmnnnoo"],
                   [5, "aaaeeeefhhmoonssrrrrttttw"],
                   [5, "aabbeeeeeeeehmosrrrruttvv"],
                   [7, "aaaaaaaaabbeeeeeeedddddggmmlloooonnssssrrrruvvyyy"]]

for challenge in challenge_cases:
    print("Start =", datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    print(challenge)
    solve(challenge[0], challenge[1])
    print("Solved = ",  datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
    print('\n-----\n')

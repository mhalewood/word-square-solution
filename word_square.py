from urllib.request import urlopen


# This function will pull the word list and filter it by the length provided
def get_length_english_words(word_length):
    request_url = "http://norvig.com/ngrams/enable1.txt"
    all_words = []
    with urlopen(request_url) as response:
        html_response = response.read()
        encoding = response.headers.get_content_charset("utf-8")
        decoded_html = html_response.decode(encoding)
        for word in decoded_html.split("\n"):
            if len(word) == word_length:
                all_words.append(word)
    return all_words


# This takes the list of available words and checks if they start with the
# prefix and can be created using only the available characters
def filter_words(unfiltered_word_list, available_characters, prefix):
    filtered_words = []
    for word in unfiltered_word_list:
        if word.startswith(prefix):
            if all(available_characters.count(char) >= word.count(char)
                   for char in word):
                filtered_words.append(word)
    return filtered_words


# This takes the characters out of the previous words to provide
# an updated list of available characters
def remove_characters(previous_words, available_characters):
    list_of_characters = list(available_characters)
    for word in previous_words:
        for char in word:
            if char in list_of_characters:
                list_of_characters.remove(char)
    return "".join(list_of_characters)


# This creates the word prefix based on the previous words,
# it also calls the remove characters function to update the list of
# available characters, it then filters the words to determine if
# there is another word that will fit and what that word or words are
def find_potential_next_words(
        previous_words, available_words, available_characters):

    prefix = ""
    for word in previous_words:
        prefix = prefix + word[len(previous_words)]

    available_characters = remove_characters(
        previous_words, available_characters)

    potential_words = filter_words(
        available_words, available_characters, prefix)

    return potential_words


# This function is recursively called until it finds all words in the word
# square or the next word is not possible and None is passed into
# the words field
def solve_recursively(words, useable_words, letters):
    if words is None:
        return
    if len(words) == len(words[0]):
        return words

    next_words = find_potential_next_words(words, useable_words, letters)

    if next_words != []:
        for word in next_words:
            new_words = words.copy()
            new_words.append(word)
            n_words = solve_recursively(new_words, useable_words, letters)
            if n_words is not None:
                return n_words


# This is the main function and will call the ge_length_english_words function
# to create the initial list of words, it passes this into the filter_words to
# reduce the list of words down to only those that mare able to be made up of
# the letters provided, then it sets each word as the first word and attempts
# to solve the word square with this as the base
def solve(word_length, letters):
    useable_words = filter_words(
        get_length_english_words(word_length), letters, '')

    for word_one in useable_words:
        complete_words = solve_recursively([word_one], useable_words, letters)
        if not (complete_words is None):
            for word in complete_words:
                print(word)
            break
    return complete_words

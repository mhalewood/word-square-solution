# Word Square Solution

## Breif of problem
This challenge is to produce a word square. In a word square you are given a grid with letters arranged that spell valid English language words when you read from left to right or from top to bottom, with the requirement that the words you spell in each column and row  f the same number are the same word. For example, the first row and the first column spell the same word, the second row and second column do, too, and so on. The challenge is that in arranging those letters that you spell valid words that meet those requirements.
One variant is where you're given an n*n grid and asked to place a set of letters inside to meet these rules, and that’s our challenge: given the grid dimensions and a list of letters, can you produce a valid word square.

## Installation
This project uses python and its built in librarys, so once python is installed there should be no other requirements

## Usage
In order to confirm the given solution provides valid responses to the challenges set, run the python file "word_square_solution.py" this has the 4 different challenges already passed into the appropriate modules, this does return a different response for the second challenge as there are 2 correct word squares for the given letters

## Description of files
### word_square_solution.py
This file loads in the solve module from word_square.py and loops through the 4 challenges and prints the output, from my testing the first 3 are completed very quickly and the final 7 letter challenge takes around 1 minute to complete, this is shown with the start & end prints for each challenge

### word_square.py
This file contains all the logic required split into the given functions, this is to be imported into any file that wants to utilise the functions.

### word_square.test.py
This file contains the unit tests that are carried out to ensure each of the functions are producing the expected results without having to rely on other functions as they would if called using the solve function, solve & recursive_solve are not tested in here as these are tested as part of word_square_solution.py